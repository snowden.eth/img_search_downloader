![garden-photo](https://images.unsplash.com/photo-1580600301354-0ce8faef576c?q=80&w=1976&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D)

# Pinterest Image Downloader 📸

## Description
This is a Python project that uses Selenium and Chromium to download images from Pinterest. It also includes a feature to increase the resolution of all images within a specified directory.

## Features

- 🌐 Web scraping using Selenium and Chromium
- 📈 Image resolution enhancer
- 📁 Bulk processing with OS library

## Requirements
- Python 3.x
- Selenium 4.11.2
- Chromium

## Installation
```bash
pip install selenium
